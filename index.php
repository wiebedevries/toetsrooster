<?php
if ($_SERVER["HTTP_HOST"] == "toetsroosters.localhost" || strpos($_SERVER["HTTP_HOST"], "test") !== false) {
    define("DEV", true);
} else {
    define("DEV", false);
}

$ENV = DEV ? require "dev.env.php" : require "prod.env.php";
define("DB_HOST", $ENV["DB_HOST"]);
define("DB_NAME", $ENV["DB_NAME"]);
define("DB_USER", $ENV["DB_USER"]);
define("DB_PASS", $ENV["DB_PASS"]);

// require necesary app files
require "app/app.php";

$no_login_needed = ["login", "aanmelden", "klas", "manual"];
$page = Request::get("page");
$handle = Request::get("handle");
$action = "";

if (strpos($page, ".") !== false) {
    $i = strpos($page, ".");
    $page = substr($page, 0, $i);
}

if ($page == "error" || ($page == "" && $handle == "")) {
    Foward::to("/login/");
}

$page_path = ($page != "") ? "app/pages/" . $page . ".page.php" : "app/handle/" . $handle . ".handle.php";
$action = ($page != "") ? $page : $handle;

Session::set("page", $page);

// first check if page exist
// then check if the user can acces the page
if (!file_exists($page_path)) {
    $page_path = "app/pages/not_found.page.php";
} else if (!in_array($action, $no_login_needed)) {
    if (DOCENT == "" || CODE == "") {
        Foward::to("/login/");
    }
}

// require the page file
require $page_path;

