<?php>
header ("P3P:CP=\"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM\"");
session_start(); 

function cleanup($string="") 
  { 
	$string = strip_tags($string); 
	$string = htmlspecialchars($string); 
	if(!get_magic_quotes_gpc()) { 
	  $string = addslashes($string); 
	} 
	return $string; 
} 

// controleren of pagina correct is aangeroepen.

include("inc_connect.php"); 

if (!isset($_SESSION['code'])) { 
	$tekst = "<font face=\"verdana\" size=\"4\">Je hebt geen geldige roostercode opgegeven.<br>Probeer opnieuw: 
	<a href=\"index.htm\"\" onmouseover=\"window.status='';return true\">Opnieuw inloggen</a></font><br>";
	die($tekst);
}else{
	
	$icode			=	$_SESSION['icode'];
	$admin 			= $_SESSION['admin'];
	$code 			= $_SESSION['code'];
	$wachtwoord = $_SESSION['wachtwoord'];
			
	date_default_timezone_set('Europe/Amsterdam');
	setlocale(LC_ALL, 'nl_NL');

	$query = "SELECT * FROM users WHERE code = '$icode' AND wachtwoord = '$wachtwoord'"; 
	$result = mysql_query($query) or die("FOUT : " . mysql_error()); 
	
	while($docentgegevens[] = mysql_fetch_array($result)); // docentinformatie
	
	$docent = $docentgegevens[0][2];
	
	if (mysql_num_rows($result) > 0){ // er bestaat een docent met code
		
		date_default_timezone_set('Europe/Amsterdam');
		setlocale(LC_ALL, 'nl_NL');

		$vandaag = strftime("%Y-%m-%d", mktime(date("j F Y")));
		$uvandaag = strftime("%A %e %B %Y", mktime(date("j F Y")));
		$week		 = intval(strftime("%W", strtotime($vandaag)))+1;	
		$_SESSION['week'] 		  = $week;
  	$_SESSION['vandaag'] 	  = $vandaag;
	
		// ophalen van door docent code opgegeven toetsen
		
		$query = "SELECT * FROM toetsopgaven WHERE code = '$code' ORDER BY klas, weekid";
		$result = mysql_query($query) or die ("FOUT: " . mysql_error());
		$aantal = mysql_num_rows($result);
		
		$toetsen = array();
		while($toetsen[] = mysql_fetch_array($result)); // data uit tabel: [index][velden]
				
	}else{
		
		echo "Ee door u ingevoerde code komt niet voor!";
		header("Location: login.php");
		exit();
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="nl" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo "Toetsoverzicht van&nbsp;" . $docent ?></title>

<script language="JavaScript">
		<!-- Begin

		function myprint() {
		window.focus();
		window.print();
		}
		//  End -->
</script>
		
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style type="text/css"> 

body{
		margin: 0;
		font-family: arial, sans-serif;
		font-size: 0.75em;
		}
 

div#header{
position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100px;
background-color: #e7ebef;
}

div#content{
padding: 110px 20px 0 20px;
background-color: #e7ebef;
}

div#content1{
	padding: 1px 1px 1px 1px;
	padding-left: 2%;
	width: 96%;
	background-color: #e7ebef;
}

div#content3{
	height: 40px;
	padding: 100px 20px 0 15px;
	padding-left: 2%;
	width: 96%;
	background-color: #e7ebef;
}
</style>

<style type="text/css" media="print"> 
		
		#content{
		display: none;
		}
		
		#header{
		display: none;
		}
		
		div.page { 
		writing-mode: tb-rl; 
		width: 100%;
		height: 100%; 
		margin: 10% 0%;
		size: landscape;
		}
</style>

</head>

<body bgcolor="#e7ebef">

<div id="header">

	<?php
	// buttons opgeven
	$actief = 1;
	$button[1][1] = "Mijn toetsen" 			 ; $button[1][2] = "overzicht.php";
	$button[2][1] = "Toetsen opgeven" 	 ; $button[2][2] = "opgeventoetsen.php";
	$button[3][1] = "Overzicht per klas" ; $button[3][2] = "toetsroosterklas.php";
	$button[4][1] = "Mijn toetsrooster"  ; $button[4][2] = "toetsroosterdocent.php";
	
	$aantalbuttons = 4;
 	?>
	
	<table border="0" width="96%" cellpadding="1" align="center">
		<tr>
			<td align="left" width="70%" valign="middle" height="40"> 
					
					<?php
					 if ($admin==0) { // geen admin
					 		echo "<font size=\"5\">" . $docent . "</font>";
					 }else{
					 		echo "<font size=\"5\" color=\"#ff0000\"><b>" . ucfirst($code) . 
									 "</b></font><font size=\"4\" color=\"#ff0000\"><i>&nbsp;&nbsp;(" . $docent . ")&nbsp;</i>";
					 }
					?>
					
					<font size="1"><i><?php echo $uvandaag?></i></font></td>
					 
			<td align="right" width="30%"><font size="5"><?php echo $button[$actief][1]?></font></td>
		</tr>
		
	</table>
	
	<table border="0" width="96%" cellpadding="1" align="center">
				
		<tr>
			<?php
			for ($i=1; $i<$aantalbuttons+1; $i++) {
					echo "<td width=\"90\" align=\"center\">";
					echo "<form style=\"margin:0;padding:0;text-decoration: none;color: navy;\" method=\"post\" ";
					echo "action=\" " . $button[$i][2] . "\" target=\"_self\" onmouseover=\"window.status='';return true\">";
					if ($actief==$i) {
						 echo "<input type=\"submit\" value=\" " . $button[$i][1] . "\" style=\"color: #bbbbbb\" ></form></td>";
					}else{
						 echo "<input type=\"submit\" value=\" " . $button[$i][1] . "\"></form></td>";
					}
			}
			
			?>
			<td width="90" align="center">
					<input type =button name="cmdPrint" value = "Afdrukken"
					onClick="myprint()" style="color: navy;" style="text-decoration: none;">
			</td>
			
			<td></td>
			
			<td align="right">
					<form style="margin:0;padding:0;text-decoration: none;color: navy;" method="post"
					 		 onmouseover="window.status='';return true"
							 action="help.htm" target="_blank" ><input type="submit" value="?">
					</form>
			</td>		
			<td width="85" align="right">
					<form style="margin:0;padding:0;text-decoration: none;color: navy;" method="post"
							 onmouseover="window.status='';return true"
							 action=" uitloggen.php" target="_self"><input type="submit" value="Uitloggen">
					</form>
			</td>
				
		</tr>
		<tr><td colspan="10"><hr></td></tr>	
	</table>
</div>

<div id="content3">
			<table width="96%" align="center"> 
					<tr valign="top" align="center">
						<td height="40" width="7%" align="left">
								<font size="4">klas</font>
						</td>
						
						<td width="53%" align="center">
								<font size="4">mijn opgegeven toetsen</font>
						</td>
						
											
						<td width="40%" align="center">
								<font size="4">opmerking / omschrijving</font><font size="2"><i><br>(optioneel)</i></font>
						</td>
					
					</tr>
				</table>
		<br>
</div>

<div id="content1">
		 	
					<?php
					
					$formverw = "<form action=\"verwijderen.php\" name=\"id\" method=\"post\">";
					$formwijz = "<form action=\"wijzigen.php\" name=\"id\" method=\"post\">";
					
					if ($aantal==0) { // als er nog geen toetsen zijn opgegeven
						 echo "<tr>\n";
						 echo "<td valign=\"top\" width=\"100\"><font size=\"5\"></font></td>";
						 echo "<td>Er zijn nog geen toetsen opgegeven!";
						 echo "<br><hr></td></tr>";
					}else{ // als er wel toetsen zijn opgegeven
						 $klas = "";
						 //echo "<tr>";
						 						 
						 for ($i=0; $i<$aantal; $i++) {  // toetsen uitlezen en in tabel plaatsen
			 			 		
								
								if (strtolower($klas) != strtolower($toetsen[$i][3])) {	 // nieuwe klas
									 $klas = $toetsen[$i][3];
									 echo "<fieldset>";
									 echo "<table width=\"100%\">";
									 echo "<tr>" . $formverw;
									 echo "<td align=\"left\" valign=\"top\" width=\"7%\"><font size=\"4\">" . ucfirst($klas) . "</font></td>";
								}else{
									 echo "<tr>" . $formverw;
									 echo "<td align=\"left\" valign=\"top\" width=\"7%\"></td>";	
								}
							
								if (strtolower($klas) == strtolower($toetsen[$i+1][3])) { 
									 $laatste=0;
								}else{
									 $laatste=1;
								}
						 		
								$id[$i] =  $toetsen[$i][0];
								
								// sprintf("% 4s",$toetsen[$i][7])
								
								$opgtoets = "week:&nbsp;"	.  $toetsen[$i][7] .
					 			  	 				"&nbsp;&nbsp;" . $toetsen[$i][4] .
									 	 				"&nbsp;&nbsp;" . $toetsen[$i][5] . "-toets" .
										 				"&nbsp;&nbsp;" . $toetsen[$i][6] . "<sup>e</sup>&nbsp;les van de week,&nbsp;&nbsp;opgegeven op:" . 
									 	 				"&nbsp;&nbsp;" . $toetsen[$i][1] . "&nbsp;&nbsp;" . 
														"<input type=\"image\" src =\"drop.jpg\" alt =\"verwijderen\" name=\"idtoets\" value= \"" . $id[$i] . "\" valign=\"bottom\">" .
														"<input type=\"hidden\" name= \"taak\" value=\"delete\">";
								
								$bood     = $toetsen[$i][9];  //  opmerking in $bood
								// if (!$bood=="") {$bood = " " . $bood;}  // voorloopspatie
																
								$opgopm		= "<input type=\"text\" name=\"opm\" size=\"50\" value=\"" . $bood . "\" style=\"background: #e7ebef; width: 305px; \">" .
														"&nbsp;&nbsp;" . 
														"<input type=\"image\" src =\"ok.jpg\" alt =\"opslaan\" name=\"idtoets\" value= \"" . $id[$i] . "\" valign=\"bottom\">" .
														"<input type=\"hidden\" name= \"taak\" value=\"change\">";					
								
								echo 				"<td align=\"left\" height=\"9\" width=\"53%\">" . 
														"<input name=\"idtoets\" type=\"hidden\" value= \"" . $id[$i] . "\">" . $opgtoets .
														"</td></form>" . 
														$formwijz . 
														"<td align=\"right\" height=\"9\" width=\"40%\">" . 
														"<input name=\"idtoets\" type=\"hidden\" value= \"" . $id[$i] . "\">" . $opgopm . 
														"</td></form></tr>";
								
								//if (strlen($toetsen[$i][9])>0) {
								//	$opmerking = "&nbsp;&nbsp;<i><font size=\"1\">(" . $toetsen[$i][9] . ")</font></i>";
								//}else {
								//	$opmerking = "";
								//}
								
								//echo strlen($toetsen[$i][9]); STYLE="color: red; background: #e7ebef;
								
								//echo $opgtoets;
								
								//echo $opmerking;
																		 
					 			if ($laatste==1) {
									 //echo "</td>";
									 echo "</tr></form></table>";
									 echo "</fieldset><br>";
									 // echo "<br>";
									 
								}else{
								// echo "<br>";
								}
								// echo "</form>";			
						}
						//echo "</td></tr>";
									
					}
					
					$_SESSION['id']=$id;
								
					?>
			
			
<br>
</div>

<!-- Start of StatCounter Code -->
<script type="text/javascript">
var sc_project=6411760; 
var sc_invisible=1; 
var sc_security="2fad64f2"; 
</script>

<script type="text/javascript"
src="http://www.statcounter.com/counter/counter.js"></script><noscript><div
class="statcounter"><a title="tumblr visitor"
href="http://statcounter.com/tumblr/" target="_blank"><img
class="statcounter"
src="http://c.statcounter.com/6411760/0/2fad64f2/1/"
alt="tumblr visitor" ></a></div></noscript>
<!-- End of StatCounter Code -->

</body>
</html>
