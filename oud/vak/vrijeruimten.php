<?php>
header ("P3P:CP=\"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM\"");
session_start(); 

function cleanup($string="") 
  { 
	$string = strip_tags($string); 
	$string = htmlspecialchars($string); 
	if(!get_magic_quotes_gpc()) { 
	  $string = addslashes($string); 
	} 
	return $string; 
} 

// controleren of pagina correct is aangeroepen.

include("inc_connect.php"); 

if (!isset($_SESSION['code'])) { 
	$tekst = "<font face=\"verdana\" size=\"4\">Je hebt geen geldige roostercode opgegeven.<br>Probeer opnieuw: 
	<a href=\"index.htm\"\" onmouseover=\"window.status='';return true\">Opnieuw inloggen</a></font><br>";
	die($tekst); 
}else{
	
	// $klas = cleanup($_POST['klas']);
	$code 			= $_SESSION['code'];
	$icode 			= $_SESSION['icode'];
	$admin      = $_SESSION['admin'];
	$wachtwoord = $_SESSION['wachtwoord'];
	
	$query = "SELECT * FROM leerlingen WHERE doc1<>'' AND (doc2 <> '' or doc3 <> '') ORDER by klas, naam, id";
	$result = mysql_query($query) or die ("FOUT: " . mysql_error());
	$aantallessen = mysql_num_rows($result);
	$lessen = array();
	while($lessen[] = mysql_fetch_array($result)); // data uit tabel:weken in $week[index][velden]
	
	$query = "SELECT * FROM users WHERE code = '$code' AND wachtwoord = '$wachtwoord'"; 
	$result = mysql_query($query) or die("FOUT : " . mysql_error()); 
	while($docentgegevens[] = mysql_fetch_array($result)); // docentinformatie
	$docent = $docentgegevens[0][2];
	if (mysql_num_rows($result) > 0){ // er bestaat een docent met code
		
		date_default_timezone_set('Europe/Amsterdam');
		setlocale(LC_ALL, 'nl_NL');

		$vandaag = strftime("%Y-%m-%d", mktime(date("j F Y")));
		$uvandaag = strftime("%A %e %B %Y", mktime(date("j F Y")));
		$hweek		 = intval(strftime("%W", strtotime($vandaag)));
		$_SESSION['week'] 		  = $week;
  	$_SESSION['vandaag'] 	  = $vandaag;
		
	
	}else{
		
		echo "De door u ingevoerde code komt niet voor!";
		header("Location: login.php");
		exit();
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="nl" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo "Vrije ruimte vakken &nbsp;"?></title>

<script language="JavaScript">
		<!-- Begin

		function myprint() {
		window.focus();
		window.print();
		}
		//  End -->
</script>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<style type="text/css"> 

a.tooltip{
		position:relative;
		padding:0 0.1em 0 0.1em}
a.tooltip x{
		position:relative;
		text-decoration:underline;
		padding:0 0.1em 0 0.1em}
a.tooltip span{
		padding:0.5em;
		display:none}
a.tooltip:hover span{
		text-decoration:none;
		display:block;
		position:absolute;
		top:-3.5em;
		left:-6.0em;
		width:13em;
		border:1px solid #d7dbdf;
		background:#f7fbff;
		color:#000000;
		font-size: 0.8em;
		text-align:center}

body{
margin: 0;
font-family: arial, sans-serif;
font-size: 0.8em;
}

div#header{
	position: fixed;
	font-family: arial, sans-serif;
	top: 0;
	left: 0;
	width: 100%;
	height: 100px;
	background-color: #e7ebef;
}
 
div#content{
	position: fixed;
	top: 100px;
	padding: 0px 20px 0 20px;
	padding-left: 2.6%;
	width: 96%;
	height: 40px;
	background-color: #e7ebef;
	font-family: arial, sans-serif;
	font-size: 1.3em;
}

div#content1{
  padding: 140px 20px 0 20px;
	padding-left: 2%;
	width: 96%;
	background-color: #e7ebef;
	font-family: arial, sans-serif;
	font-size: 1.0em;
}

</style>

<style type="text/css" media="print"> 
		
		#content{
		display: none;
		}
		
		#header{
		display: none;
		}
		
		div.page { 
		writing-mode: tb-rl; 
		width: 100%;
		height: 100%; 
		margin: 5% 0%;
		size: landscape;
		}
		
		#footer{
  	display:none;
}

</style>

</head>

<body bgcolor="#e7ebef">
 
<div id="header">

	<?php
	// buttons opgeven
	$actief = 6;
	$button[1][1] = "Mijn toetsen" 			 	 ; $button[1][2] = "overzicht.php";
	$button[2][1] = "Toetsen opgeven" 	 	 ; $button[2][2] = "opgeventoetsen.php";
	$button[3][1] = "Overzicht per klas" 	 ; $button[3][2] = "toetsroosterklas.php";
	$button[4][1] = "Mijn toetsrooster"  	 ; $button[4][2] = "toetsroosterdocent.php";
	$button[5][1] = "Mentorgroepen"  		 	 ; $button[5][2] = "lesgroepenmentor.php";
	$button[6][1] = "Vrije ruimte vakken"  ; $button[6][2] = "vrijeruimten.php";
	
	$aantalbuttons = 4;
 	?>
	
	<table border="0" width="96%" cellpadding="1" align="center">
		<tr>
			<td align="left" width="70%" valign="middle" height="40">
					
					<?php
					 if ($admin==0) { // geen admin
					 		echo "<font size=\"5\">" . $docent . "</font>";
					 }else{
					 		echo "<font size=\"5\" color=\"#ff0000\"><b>" . ucfirst($code) . 
									 "</b></font><font size=\"4\" color=\"#ff0000\"><i>&nbsp;&nbsp;(" . $docent . ")&nbsp;</i>";
					 }
					?>
					
					<font size="1"><i><?php echo $uvandaag?></i></font></td>
					 
			<td align="right" width="30%"><font size="5"><?php echo $button[$actief][1]?></font></td>
		</tr>
		
	</table>

	<table border="0" width="96%" cellpadding="1" align="center">
				
		<tr>
			<?php
			
			for ($i=1; $i<$aantalbuttons+1; $i++) {
					echo "<td align=\"center\" width=\"90\">";
					echo "<form style=\"margin:0;padding:0;text-decoration: none;color: navy;\" method=\"post\" ";
					echo "action=\" " . $button[$i][2] . "\" target=\"_self\" onmouseover=\"window.status='';return true\">";
					if ($actief==$i) {
						 echo "<input type=\"submit\" value=\" " . $button[$i][1] . "\" style=\"color: #bbbbbb\" ></form></td>";
					}else{
						 echo "<input type=\"submit\" value=\" " . $button[$i][1] . "\"></form></td>";
					}
			}
			echo "<td></td>";
			?>
			
			<td width="90" align="center">
					<input type =button name="cmdPrint" value = "Afdrukken"
					onClick="myprint()" style="color: navy;" style="text-decoration: none;">
			</td>
				
			<td width="90" align="right">
			<?php
			if ($code=="vre") {
				 echo "<form style=\"margin:0;padding:0;text-decoration: none;color: navy;\" method=\"post\"" .
							"onmouseover=\"window.status='';return true\"" . 
							"action=\"vrijeruimten.php\" target=\"_self\"><input type=\"submit\" value=\"V\">" . 
							"</form>";
			}
			?>
			</td>
				
			<td width="20" align="right">
					<form style="margin:0;padding:0;text-decoration: none;color: navy;" method="post"
							 onmouseover="window.status='';return true"
							 action="lesgroependocent.php" target="_self"><input type="submit" value="G">
					</form>
			</td>	
					
			<td width="90" align="right">
					<form style="margin:0;padding:0;text-decoration: none;color: navy;" method="post"
							 onmouseover="window.status='';return true"
							 action="uitloggen.php" target="_self"><input type="submit" value="Uitloggen">
					</form>
			</td>
					
		</tr>
		<tr><td colspan="10"><hr></td></tr>
	</table>	
</div>
<div id="content">
	<table width="100%">
			<tr align="center"><font size="4">
					 <td width="5%" align="left"></td>
					 <td width="20%" align="left"></td>
					 <td width="10%" align="left">tijd</td>
					 <td width="10%" align="left">rooster</td>
					 <td width="10%" align="left">2</td>
					 <td width="10%" align="left">3</td>
					 <td width="5%"></td>
					 <td width="30%" align="left">wijziging</td>
			</tr></font>
			
	</table>
	
</div>

<div id="content1">
	<fieldset>
		<table width="100%"> 					 	
			 <tr><font size="4">
			 		 <td width="5%" align="left"></td>
					 <td width="20%" align="left"></td>
					 <td width="10%" align="left"></td>
					 <td width="10%"  align="left"></td>
					 <td width="10%"  align="left"></td>
					 <td width="10%"  align="left"></td>
					 <td width="5%"></td>
					 <td width="30%"></td>
			 </tr></font>
			 
			 <?php
			 
			 $dag[1] = "ma";
			 $dag[2] = "di";
			 $dag[3] = "wo";
			 $dag[4] = "do";
			 $dag[5] = "vr";
						
			 $formverw = "<form action=\"verwijderen.php\" name=\"id\" method=\"post\">";
			 $formwijz = "<form action=\"wijzigen.php\" name=\"id\" method=\"post\">";
					
			 for ($i=0; $i<$aantallessen; $i++) {  // lessen uitlezen en in tabel plaatsen
			 		$regel = "";	 		
			 		if (strtolower($klas) != strtolower($lessen[$i][2])) {	// volgende klas
						 $klas = $lessen[$i][2];
						 $regel = $regel . "<td><b>" . $klas . "</td></b>";
					}else{
						 $regel = $regel . "<td></td>";
					}
					if (strtolower($naam) != strtolower($lessen[$i][1])) {  // volgende leerling
						 		$naam = $lessen[$i][1];
								$regel = $regel . "<td>" . $naam . "</td>";
					}else{
						 $regel = $regel . "<td></td>";
					}
					$regel = $regel . "<td>" .$dag[$lessen[$i][3]] . " " . 
								 	 $lessen[$i][4] . "</td><td>" . $lessen[$i][5] . "</td><td>" . 
									 $lessen[$i][7] . "</td><td>" . $lessen[$i][9];
					echo "<tr>" . $regel . "</tr>";
					if (strtolower($klas) != strtolower($lessen[$i+1][2])) {
						 echo "<tr><td colspan=\"8\" height=\"1%\"><hr></td></tr>";
					}
			 }
			 
			 ?>
		</table>		
	</fieldset>										
</div>
		
</body>
</html>

