<?php>
header ("P3P:CP=\"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM\"");
session_start(); 

function cleanup($string="") 
  { 
	$string = strip_tags($string); 
	$string = htmlspecialchars($string); 
	if(!get_magic_quotes_gpc()) { 
	  $string = addslashes($string); 
	} 
	return $string; 
} 

// controleren of pagina correct is aangeroepen.

include("inc_connect.php"); 

if (!isset($_SESSION['code'])) { 
	$tekst = "<font face=\"verdana\" size=\"4\">Je hebt geen geldige roostercode opgegeven.<br>Probeer opnieuw: 
	<a href=\"index.htm\"\" onmouseover=\"window.status='';return true\">Opnieuw inloggen</a></font><br>";
	die($tekst); 
}else{
	
	$klas = cleanup($_POST['klas']);
	$code 			= $_SESSION['code'];
	$icode 			= $_SESSION['icode'];
	$admin      = $_SESSION['admin'];
	$wachtwoord = $_SESSION['wachtwoord'];
	
	$query = "SELECT DISTINCT klas FROM rooster WHERE docent = '$code' AND vak = 'mtu'";
	$result = mysql_query($query) or die ("FOUT: " . mysql_error());
	$aantalklassen = mysql_num_rows($result);
	$klassen = array();
	while($klassen[] = mysql_fetch_array($result)); // data uit tabel:weken in $week[index][velden]
	
	if ($klas=="") {
		 $klas=$klassen[0][0];
	}
	
	if ($klas=="") {
		 $query = "SELECT klas FROM rooster WHERE docent = '$code' ORDER BY klas" ;
		 $result = mysql_query($query) or die ("FOUT: " . mysql_error());
		 $aantalklassen = mysql_num_rows($result);
		 $klassen = array();
		 while($klassen[] = mysql_fetch_array($result)); // data uit tabel:weken in $week[index][velden]	 
	}
	
	if ($klas=="") {
		 $klas=$klassen[0][0];
	}
	
	if ($klas=="") {
		 $klas="Mh1a";
	}
	
	if ($klas=="A2a") {
		 $klas = "G2a";
	}
		
	$query = "SELECT * FROM users WHERE code = '$icode' AND wachtwoord = '$wachtwoord'"; 
	$result = mysql_query($query) or die("FOUT : " . mysql_error()); 
	
	while($docentgegevens[] = mysql_fetch_array($result)); // docentinformatie
	
	$docent = $docentgegevens[0][2];
	
	if (mysql_num_rows($result) > 0){ // er bestaat een docent met code
		
		date_default_timezone_set('Europe/Amsterdam');
		setlocale(LC_ALL, 'nl_NL');

		$vandaag = strftime("%Y-%m-%d", mktime(date("j F Y")));
		$uvandaag = strftime("%A %e %B %Y", mktime(date("j F Y")));
		$hweek		 = intval(strftime("%W", strtotime($vandaag)));
		$_SESSION['week'] 		  = $week;
  	$_SESSION['vandaag'] 	  = $vandaag;
				
		//  id ophalen van huidige week
		
		$query = "SELECT * FROM weken WHERE week = '$hweek'";
		$result = mysql_query($query) or die ("FOUT: " . mysql_error());
		
		$weken = array();
		while($weken[] = mysql_fetch_array($result)); // data uit tabel:weken in $week[index][velden]
		$hweekid = $weken[0][0];  // eventueel + getal achter [0][0] voor testen aantal weken verder
		
		$query = "SELECT * FROM weken WHERE id >= '$hweekid'";
		$result = mysql_query($query) or die ("FOUT: " . mysql_error());
		$aantalweken = mysql_num_rows($result);
		
		$weken = array();
		while($weken[] = mysql_fetch_array($result)); // data uit tabel:weken in $week[index][velden]
		
		$vweek = $weken[3][1];
		
		if (!isset($_POST['cweek'])) {  //  cweek = gekozenweek
			 $cwid  = $hweekid;  //  als nog geen week gekozen
			 $cweek = $hweek;    
		}else{ 
			 $cweek=cleanup($_POST["cweek"]);
		}
		
		for ($f=0; $f<$aantalweken; $f++) { 
			 if ($weken[$f][1]==$cweek) {
  		 		$cwid = $weken[$f][0];
			 }
			 															
		}
			
		// ophalen van het toetsrooster van de klas voor de 4 eerstvolgende weken
									
		$optehalenweek = 42;  //  er moet een geldig rooster van deze week in de tabel staan
		
		$query = "SELECT * FROM rooster WHERE week = '$optehalenweek' AND klas = '$klas' ORDER BY id";
		$result = mysql_query($query) or die ("FOUT: " . mysql_error());
		$aantallessen = mysql_num_rows($result);
				
		if (mysql_num_rows($result) > 0) {
		
			 $rooster= array();
			 while($rooster[] = mysql_fetch_array($result)); // data uit tabel:date in $klasrooster[index][velden]
		}
					
		$eersteweekid = $cwid;
		$laatsteweekid = $cwid + 3;
		
		$query = "SELECT * FROM toetsopgaven WHERE weekid >= '$eersteweekid' AND weekid <= '$laatsteweekid' And klas = '$klas' ORDER BY week";  // opgegeven toetsen ophalen
		$result = mysql_query($query) or die ("FOUT: " . mysql_error());
		$aantaltoetsen = mysql_num_rows($result);
	
		if (mysql_num_rows($result) > 0) {
		
			 $toetsen= array();
			 while($toetsen[] = mysql_fetch_array($result)); // data uit tabel:date in $toetsen[index][velden]
				
		}	
		
		//  echo $toetsen[0][2] . "gewicht:" . $toetsen[0][5] . "les:" . $toetsen[0][6] . "week:" . $toetsen[0][7] . "<br>";
		//  echo $toetsen[1][2] . "gewicht:" . $toetsen[1][5] . "les:" . $toetsen[1][6] . "week:" . $toetsen[1][7] . "<br>";
		//  echo $toetsen[2][2] . "gewicht:" . $toetsen[2][5] . "les:" . $toetsen[2][6] . "week:" . $toetsen[2][7] . "<br>";
		
		// for ($t=0; $t<$aantaltoetsen; $t++)  {  //  maak lvdw (les van de week) allemaal 0
		//		echo $toetsen[$t][2] . $toetsen[$t][3] . $toetsen[$t][4] . "gew:" . $toetsen[$t][6] . "<br>";
		// }
		
		$query = "SELECT * FROM weken WHERE id >= '$cwid' ORDER BY id LIMIT 0,4";  // weekinfo ophalen
		$result = mysql_query($query) or die ("FOUT: " . mysql_error());
			
		if (mysql_num_rows($result) > 0) {
		
			 $data= array();
			 while($data[] = mysql_fetch_array($result)); // data uit tabel:date in $data[index][velden]
				
		}
		
		for ($t=0; $t<4; $t++)  {  //  lesrooster overnemen voor volgende 3 weken
				$klasrooster[$t] = $rooster;
		}	
				
		//  opgehaalde toetsen in rooster plaatsen
		
		for ($t=0; $t<$aantaltoetsen; $t++)  {  //  maak lvdw (les van de week) allemaal 0
				$lvdw[$t] = 0;
		}				 
		
		for ($w=0; $w<4; $w++) {  // 4 weken ophalen te beginnen met deze week en toetsen in rooster plaatsen 
				
				$wk = $data[$w][1];  //  weeknummer lezen
				
				for ($les=0; $les<$aantallessen; $les++) {  //  alle lessen van de klas van deze week aflopen
				
						$rd = strtolower($klasrooster[$w][$les][5]);  //  docent uit het rooster
						$rv = strtolower($klasrooster[$w][$les][4]);  //  vak uit rooster
												
						for ($t=0; $t<$aantaltoetsen; $t++) {  //  alle opgegeven toetsen aflopen
														
								if ($toetsen[$t][7] == ($wk)) {  //  als week klopt
									 if ($rd == strtolower($toetsen[$t][2])) {	//  docent uit rooster is docent van toets
									 		//  echo "docent klopt" . ($w + $week) . ":" . $les . ":" . $t  . "<br>";
											// echo $w . ":" . $klasrooster[$w][$les][5] . "dag:" . $klasrooster[$w][$les][2] . "uur:" . $klasrooster[$w][$les][3] . "aantal: " . $lvdw[$t] . "<br>";
											//  echo $lvdw[$t];
											if ( ($rv == strtolower($toetsen[$t][4])) or ($toetsen[$t][4])=="") {  //  vak uit rooster is vak toets
									 			 // echo "week en docent klopt";
												 $lvdw[$t] = $lvdw[$t] + 1;
									 			 if (($toetsen[$t][6]) == $lvdw[$t]) {  // juiste les van de week
												 		$klasrooster[$w][$les][7] = $toetsen[$t][5];
														if (!($toetsen[$t][9]=="")) {$klasrooster[$w][$les][8] = $toetsen[$t][9];}  //  opmerking van toets naar rooster
													}
									 		}
									 }
								}
						}  //  einde lus voor aflopen toetsen						
						
				}  //  einde lus voor lessen van de week
					 
		}  //  einde weeklus
	}else{
		
		echo "De door u ingevoerde code komt niet voor!";
		header("Location: login.php");
		exit();
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="nl" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo "Toetsrooster van&nbsp;" . ucfirst($klas) ?></title>

<script language="JavaScript">
		<!-- Begin

		function myprint() {
		window.focus();
		window.print();
		}
		//  End -->
</script>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<style type="text/css"> 

a.tooltip{
		position:relative;
		padding:0 0.1em 0 0.1em}
a.tooltip x{
		position:relative;
		text-decoration:underline;
		padding:0 0.1em 0 0.1em}
a.tooltip span{
		padding:0.5em;
		display:none}
a.tooltip:hover span{
		text-decoration:none;
		display:block;
		position:absolute;
		top:-3.5em;
		left:-6.0em;
		width:13em;
		border:1px solid #d7dbdf;
		background:#f7fbff;
		color:#000000;
		font-size: 0.8em;
		text-align:center}

body{
		margin: 0;
		font-family: arial narrow, sans-serif;
		font-size: 0.75em;
		}
				
body1{
margin: 0;
font-family: Verdana, sans-serif;
font-size: 0.8em;
}

div#header{
position: fixed;
font-family: arial, sans-serif;
top: 0;
left: 0;
width: 100%;
height: 100px;
background-color: #e7ebef;
}
 
div#content{
padding: 100px 20px 0 20px;
padding-left: 2%;
height: 40px;
background-color: #e7ebef;
}

div#content1{
  padding: 0px 20px 0 20px;
	padding-left: 2%;
	width: 96%;
	background-color: #e7ebef;
}

</style>

<style type="text/css" media="print"> 
		
		#content{
		display: none;
		}
		
		#header{
		display: none;
		}
		
		div.page { 
		writing-mode: tb-rl; 
		width: 100%;
		height: 100%; 
		margin: 10% 0%;
		size: landscape;
		}
		
		#footer{
  	display:none;
}

 

</style>

</head>

<body bgcolor="#e7ebef">
 
<div id="header">

	<?php
	// buttons opgeven
	$actief = 3;
	$button[1][1] = "Mijn toetsen" 			 ; $button[1][2] = "overzicht.php";
	$button[2][1] = "Toetsen opgeven" 	 ; $button[2][2] = "opgeventoetsen.php";
	$button[3][1] = "Overzicht per klas" ; $button[3][2] = "toetsroosterklas.php";
	$button[4][1] = "Mijn toetsrooster"  ; $button[4][2] = "toetsroosterdocent.php";
	
	$aantalbuttons = 4;
 	?>
	
	<table border="0" width="96%" cellpadding="1" align="center">
		<tr>
			<td align="left" width="70%" valign="middle" height="40">
					
					<?php
					 if ($admin==0) { // geen admin
					 		echo "<font size=\"5\">" . $docent . "</font>";
					 }else{
					 		echo "<font size=\"5\" color=\"#ff0000\"><b>" . ucfirst($code) . 
									 "</b></font><font size=\"4\" color=\"#ff0000\"><i>&nbsp;&nbsp;(" . $docent . ")&nbsp;</i>";
					 }
					?>
					
					<font size="1"><i><?php echo $uvandaag?></i></font></td>
					 
			<td align="right" width="30%"><font size="5"><?php echo $button[$actief][1]?></font></td>
		</tr>
		
	</table>
	
	<table border="0" width="96%" cellpadding="1" align="center">
				
		<tr>
			<?php
			
			for ($i=1; $i<$aantalbuttons+1; $i++) {
					echo "<td align=\"center\" width=\"90\">";
					echo "<form style=\"margin:0;padding:0;text-decoration: none;color: navy;\" method=\"post\" ";
					echo "action=\" " . $button[$i][2] . "\" target=\"_self\" onmouseover=\"window.status='';return true\">";
					if ($actief==$i) {
						 echo "<input type=\"submit\" value=\" " . $button[$i][1] . "\" style=\"color: #bbbbbb\" ></form></td>";
					}else{
						 echo "<input type=\"submit\" value=\" " . $button[$i][1] . "\"></form></td>";
					}
			}
			echo "<td></td>";
			?>
			
			<td width="90" align="center">
					<input type =button name="cmdPrint" value = "Afdrukken"
					onClick="myprint()" style="color: navy;" style="text-decoration: none;">
			</td>
				
			<td></td>		
			<td align="right">
					<form style="margin:0;padding:0;text-decoration: none;color: navy;" method="post"
							onmouseover="window.status='';return true"
							action=" uitloggen.php" target="_self"><input type="submit" value="Uitloggen">
					</form>
			</td>
					
		</tr>
		<tr><td colspan="10"><hr></td></tr>	
	</table>
</div>
 
<div id="content">
			<form method="post" action="toetsroosterklas.php "alt="">
				<table width="96%"> 
					<tr align="center" valign="middle">
						<td valign="middle" height="15" width="10%">
							<select name="klas" size="1" onchange="this.form.submit()">
								<?php	if (!($klas=="")) {echo "<option>" . ucfirst($klas) . "</option>";}?>
								<option>B1a</option>
								<option>B1b</option>
								<option>B1c</option>
								<option>K1a</option>
								<option>K1b</option>
								<option>K1c</option>
								<option>B2a</option>
								<option>B2b</option>
								<option>B2c</option>
								<option>K2a</option>
								<option>K2b</option>
								<option>K2c</option>
							</select>				
						</td>
						
						<td valign="middle" height="15" width="10%">
							<select name="cweek" size="1" onchange="this.form.submit()">
								<?php
								echo "<option>" . $cweek ."</option>";
								for ($f=0; $f<$aantalweken; $f++) { 
										if (!($weken[$f][1]==$cweek)) {
											 echo "<option>" . $weken[$f][1] . "</option><br>";
										}
								}
								?>
							</select>				
						</td>
						
						<td width="20%"></td>
						
						<?if ($klas == "G2a") {
								 $kl = "G2a (en A2a)";
						}else{
								 $kl = $klas;
						}
						?>
						
						<td width="60%" align="right" valign="middle" ><font size="5"><?php echo ucfirst($kl) ?></font>
						</td>
						
					</tr>
				</table>
			</form>
		<br>

		</div>	
	<div id="content1">
		<fieldset>
		<table> 
			<font size="2">
				<tr>
					<td width="5%" height="25"></td>
					<td width="19%" colspan="9"><font size="4">Maandag</font></td>
					<td width="19%" colspan="9"><font size="4">Dinsdag</font></td>
					<td width="19%" colspan="9"><font size="4">Woensdag</font></td>
					<td width="19%" colspan="9"><font size="4">Donderdag</font></td>
					<td width="19%" colspan="9"><font size="4">Vrijdag</font></td>
				</tr>
		
				<tr align="center">
					<td height="8"></td>
					<td width="2%">1</td><td width="2%">2</td><td width="2%">3</td><td width="2%">4</td>
					<td width="2%">5</td><td width="2%">6</td><td width="2%">7</td><td>8</td><td width="3%"></td>
			
					<td width="2%">1</td><td width="2%">2</td><td width="2%">3</td><td width="2%">4</td>
					<td width="2%">5</td><td width="2%">6</td><td width="2%">7</td><td>8</td><td width="3%"></td>
				
					<td width="2%">1</td><td width="2%">2</td><td width="2%">3</td><td width="2%">4</td>
					<td width="2%">5</td><td width="2%">6</td><td width="2%">7</td><td>8</td><td width="3%"></td>
				
					<td width="2%">1</td><td width="2%">2</td><td width="2%">3</td><td width="2%">4</td>
					<td width="2%">5</td><td width="2%">6</td><td width="2%">7</td><td>8</td><td width="3%"></td>
				
					<td width="2%">1</td><td width="2%">2</td><td width="2%">3</td><td width="2%">4</td>
					<td width="2%">5</td><td width="2%">6</td><td width="2%">7</td><td>8</td><td width="3%"></td>
				</tr>
		
							
				<?php 
			
				$toetsvrij = 0;
				
				for ($w=0; $w<4; $w++) {  // 4 weken ophalen te beginnen met deze week 
					
					$idteller = 0;
					$wk = $data[$w][1];  //  weeknummer lezen
					
					echo "<tr>";
					echo 				"<td colspan=\"45\"><hr></td>";
					echo "</tr>";
					
					$time = strtotime($data[$w][2]);
					
					$aweek = $wk;
										
					echo "<tr>";
					echo 				"<td colspan =\"1\" align=\"left\" height=\"12\"><font size=\"4\"><b>" . $aweek . "</b></font></td><td colspan =\"5\" align=\"left\"><font size=\"2\"><i><u>". strftime('%e %B', $time) . "</u></i></font></td><td colspan =\"18\" ><font size=\"2\" color=\"#ff0000\"><i><b>" . $data[$w][4] . "</i></b></font></td>";
					echo "</tr>";
					//  '%a. %e-%m'  oude
					echo "<tr>";
					echo 				"<td colspan=\"45\" height=\"10\"></td>";
					echo "</tr>";
					
					echo "<tr>";
					echo 				"<td align=\"left\" height=\"10\"></td>";
					echo "<font size=\"3\">";
					
					$dag[1] = "Maandag";
					$dag[2] = "Dinsdag";
					$dag[3] = "Woensdag";
					$dag[4] = "Donderdag";
					$dag[5] = "Vrijdag";
					
					for ($d=1; $d<6; $d++) {  // alle dagen van de week aflopen
				
						for ($u=1; $u<9; $u++) {  // alle uren van de dag aflopen
							// echo $d . ":" . $klasrooster[$idteller][3] . ":" . $u;
							if($klasrooster[$w][$idteller][3]==$u) {  // als uur van les uit rooster overeenkomt met plaats in tabel
								// echo $d . ":" . $klasrooster[$idteller][3] . ":" . $u;
								if ($data[$w][$d+5]>1) {  //  als er lesvrije of toetsvrije dagen zijn
									 if ($data[$w][$d+5]==1) { 
									 		echo "<td valign=\"top\" align=\"center\"><font size=\"2\" color=\"#808080\"><i>" . $klasrooster[$w][$idteller][4] . "</i></font></td>";
											if ($toetsvrij==0)  {
												 $toetsvrij = 1;
												 $boodschap = "* ". $dag[$d] . " in week " . $wk . " is een toetsvrije lesdag!";
											}
									 }
									 if ($data[$w][$d+5]==2) { 
									 		if ($u==2)  {
												 echo "<td valign=\"top\" align=\"center\"><i>vrij</i></td>";
											}else{
												 echo "<td valign=\"top\" align=\"center\"><i></i></td>";
											}
									 }
									 $idteller = $idteller + 1; 
								}else{	
								
								
									 if ($klasrooster[$w][$idteller][7]>0) {  // als er een toets gepland staat, gewicht aangeven
								 	 		$tekst = "<br>" . $klasrooster[$w][$idteller][7];
											if (!isset($klasrooster[$w][$idteller][8])) {  //  geen opmerking
									 			 echo "<td valign=\"top\" align=\"center\"><font size=\"3\"><b><u>" . $klasrooster[$w][$idteller][4] . "</u>" . $tekst . "</b></font></td>";
											}else{
												 echo "<td valign=\"top\" align=\"center\"><font size=\"3\"><b><u><a href=\"#\" class=\"tooltip\">" . 
												 $klasrooster[$w][$idteller][4] . "<span>" . $klasrooster[$w][$idteller][4] . ": " . 
												 ucfirst($klasrooster[$w][$idteller][8]) . "</span></a></u>" . $tekst . "</b></font></td>"; 
											}
									 }else{
									 		$tekst = "";
									 		echo "<td valign=\"top\" align=\"center\"><font size=\"2\">" . $klasrooster[$w][$idteller][4] . $tekst . "</font></td>";
									 }
								
									 $idteller = $idteller + 1;
								}
							}else{  // geen les in rooster, dus lege cel plaatsten
								echo "<td></td>";
							}
			
						}
						if ($d<5) { echo "<td>|</td>"; }
					}					
					echo "</tr>";
								
				}
				
				?>
				
			</font>
					
		</table>
		</fieldset>	
	</div>	
		
		<?php
		
		echo "<tr><br>";
		echo "<td colspan =\"25\" align=\"left\" height=\"15\"><i>&nbsp;&nbsp;&nbsp;Vanaf week " . $vweek . "&nbsp;is het een voorlopig rooster en kan dus nog wijzigen!</i></td><br>";
		//  echo $toetsvrij;
		if (!($toetsvrij=0))  {
			 echo "<td colspan =\"25\" align=\"left\" height=\"15\"><font color=\"#808080\"><i>&nbsp;&nbsp;&nbsp;" . $boodschap . "</i></font></td>";
		}
		echo "</tr>";
		
		?>
		
		</div>	
</body>
</html>

