<?php>
header ("P3P:CP=\"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM\"");
session_start(); 

function cleanup($string="") 
  { 
	$string = strip_tags($string); 
	$string = htmlspecialchars($string); 
	if(!get_magic_quotes_gpc()) { 
	  $string = addslashes($string); 
	} 
	return $string; 
} 

// controleren of pagina correct is aangeroepen.

include("inc_connect.php"); 

if (!isset($_SESSION['code'])) { 
	$tekst = "<font face=\"verdana\" size=\"4\">Je hebt geen geldige roostercode opgegeven.<br>Probeer opnieuw: 
	<a href=\"index.htm\"\" onmouseover=\"window.status='';return true\">Opnieuw inloggen</a></font><br>";
	die($tekst); 
}else{
	
	// $klas = cleanup($_POST['klas']);
	$code 			= $_SESSION['code'];
	$icode 			= $_SESSION['icode'];
	$admin      = $_SESSION['admin'];
	$wachtwoord = $_SESSION['wachtwoord'];
	
	$query = "SELECT DISTINCT klas FROM leerlingen ORDER by klas";
	$result = mysql_query($query) or die ("FOUT: " . mysql_error());
	$aantalklassen = mysql_num_rows($result);
	$klassen = array();
	while($klassen[] = mysql_fetch_array($result)); // data uit tabel:weken in $week[index][velden]
	
	if (!isset($_POST['cklas'])) {
		 $cklas = $klassen[0][0];
	}else{
		 $cklas = $_POST['cklas'];
	}
	
	// $cklas = "h4a";
	
	$query = "SELECT * FROM leerlingen WHERE klas = '$cklas' ORDER BY naam, id";
	$result = mysql_query($query) or die ("FOUT: " . mysql_error());
	$aantalleerlingen = mysql_num_rows($result);
	$leerlingen = array();
	while($leerlingen[] = mysql_fetch_array($result)); // data uit tabel:weken in $week[index][velden]
	
	$query = "SELECT * FROM users WHERE code = '$code' AND wachtwoord = '$wachtwoord'"; 
	$result = mysql_query($query) or die("FOUT : " . mysql_error()); 
	while($docentgegevens[] = mysql_fetch_array($result)); // docentinformatie
	$docent = $docentgegevens[0][2];
	if (mysql_num_rows($result) > 0){ // er bestaat een docent met code
		
		date_default_timezone_set('Europe/Amsterdam');
		setlocale(LC_ALL, 'nl_NL');

		$vandaag = strftime("%Y-%m-%d", mktime(date("j F Y")));
		$uvandaag = strftime("%A %e %B %Y", mktime(date("j F Y")));
		$hweek		 = intval(strftime("%W", strtotime($vandaag)));
		$_SESSION['week'] 		  = $week;
  	$_SESSION['vandaag'] 	  = $vandaag;
		
	
	}else{
		
		echo "De door u ingevoerde code komt niet voor!";
		header("Location: login.php");
		exit();
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="nl" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo "Mentoroverzicht van &nbsp;" . ucfirst($cklas) ?></title>

<script language="JavaScript">
		<!-- Begin

		function myprint() {
		window.focus();
		window.print();
		}
		//  End -->
</script>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<style type="text/css"> 

a.tooltip{
		position:relative;
		padding:0 0.1em 0 0.1em}
a.tooltip x{
		position:relative;
		text-decoration:underline;
		padding:0 0.1em 0 0.1em}
a.tooltip span{
		padding:0.5em;
		display:none}
a.tooltip:hover span{
		text-decoration:none;
		display:block;
		position:absolute;
		top:-3.5em;
		left:-6.0em;
		width:13em;
		border:1px solid #d7dbdf;
		background:#f7fbff;
		color:#000000;
		font-size: 0.8em;
		text-align:center}

body{
		margin: 0;
		font-family: arial narrow, sans-serif;
		font-size: 0.75em;
		}
				
body1{
margin: 0;
font-family: Verdana, sans-serif;
font-size: 0.8em;
}

div#header{
position: fixed;
font-family: arial, sans-serif;
top: 0;
left: 0;
width: 100%;
height: 100px;
background-color: #e7ebef;
}
 
div#content{
padding: 100px 20px 0 20px;
padding-left: 2%;
height: 40px;
background-color: #e7ebef;
}

div#content1{
  padding: 0px 20px 0 20px;
	padding-left: 2%;
	width: 96%;
	background-color: #e7ebef;
	font-family: arial narrow;
	font-size: 0.9em;
}

</style>

<style type="text/css" media="print"> 
		
		#content{
		display: none;
		}
		
		#header{
		display: none;
		}
		
		div.page { 
		writing-mode: tb-rl; 
		width: 100%;
		height: 100%; 
		margin: 5% 0%;
		size: landscape;
		}
		
		#footer{
  	display:none;
}

</style>

</head>

<body bgcolor="#e7ebef">
 
<div id="header">

	<?php
	// buttons opgeven
	$actief = 5;
	$button[1][1] = "Mijn toetsen" 			 ; $button[1][2] = "overzicht.php";
	$button[2][1] = "Toetsen opgeven" 	 ; $button[2][2] = "opgeventoetsen.php";
	$button[3][1] = "Overzicht per klas" ; $button[3][2] = "toetsroosterklas.php";
	$button[4][1] = "Mijn toetsrooster"  ; $button[4][2] = "toetsroosterdocent.php";
	$button[5][1] = "Mentorgroepen"  		 ; $button[5][2] = "lesgroepenmentor.php";
	
	$aantalbuttons = 4;
 	?>
	
	<table border="0" width="96%" cellpadding="1" align="center">
		<tr>
			<td align="left" width="70%" valign="middle" height="40">
					
					<?php
					 if ($admin==0) { // geen admin
					 		echo "<font size=\"5\">" . $docent . "</font>";
					 }else{
					 		echo "<font size=\"5\" color=\"#ff0000\"><b>" . ucfirst($code) . 
									 "</b></font><font size=\"4\" color=\"#ff0000\"><i>&nbsp;&nbsp;(" . $docent . ")&nbsp;</i>";
					 }
					?>
					
					<font size="1"><i><?php echo $uvandaag?></i></font></td>
					 
			<td align="right" width="30%"><font size="5"><?php echo $button[$actief][1]?></font></td>
		</tr>
		
	</table>

	<table border="0" width="96%" cellpadding="1" align="center">
				
		<tr>
			<?php
			
			for ($i=1; $i<$aantalbuttons+1; $i++) {
					echo "<td align=\"center\" width=\"90\">";
					echo "<form style=\"margin:0;padding:0;text-decoration: none;color: navy;\" method=\"post\" ";
					echo "action=\" " . $button[$i][2] . "\" target=\"_self\" onmouseover=\"window.status='';return true\">";
					if ($actief==$i) {
						 echo "<input type=\"submit\" value=\" " . $button[$i][1] . "\" style=\"color: #bbbbbb\" ></form></td>";
					}else{
						 echo "<input type=\"submit\" value=\" " . $button[$i][1] . "\"></form></td>";
					}
			}
			echo "<td></td>";
			?>
			
			<td width="90" align="center">
					<input type =button name="cmdPrint" value = "Afdrukken"
					onClick="myprint()" style="color: navy;" style="text-decoration: none;">
			</td>
				
			<td width="90"></td>
				
			<td width="90" align="right">
					<form style="margin:0;padding:0;text-decoration: none;color: navy;" method="post"
							 onmouseover="window.status='';return true"
							 action="lesgroependocent.php" target="_self"><input type="submit" value="G">
					</form>
			</td>	
					
			<td width="90" align="right">
					<form style="margin:0;padding:0;text-decoration: none;color: navy;" method="post"
							 onmouseover="window.status='';return true"
							 action="uitloggen.php" target="_self"><input type="submit" value="Uitloggen">
					</form>
			</td>
					
		</tr>
		<tr><td colspan="10"><hr></td></tr>	
	</table>
</div>

<div id="content">

			<form method="post" action="lesgroepenmentor.php "alt="">
				<table width="96%"> 
					<tr valign="middle">
						<td width="1%"></td>
						<td valign="middle" height="15" width="10%" align="left" >
							<select name="cklas" size="1" onchange="this.form.submit()">
								<?php
								echo "<option>" . $cklas ."</option>";
								for ($i=0; $i<$aantalklassen; $i++) {
										if (!($klassen[$i][0] == $cklas)) {
										echo "<option>" . $klassen[$i][0] . "</option><br>";
										}
								}
							?>	
							</select>				
						</td>
						
						<td valign="middle" height="15" width="10%"></td>
						
						<td width="20%"></td>
								
					</tr>
				</table>
			</form>
		<br>
</div>	

<div id="content1">
	<fieldset>
		<table> 
			<font size="2">
				<tr>
					<td width="12%" height="25">
					<td width="16%" colspan="10"><font size="4">Maandag</font></td>
					<td width="16%" colspan="10"><font size="4">Dinsdag</font></td>
					<td width="16%" colspan="10"><font size="4">Woensdag</font></td>
					<td width="16%" colspan="10"><font size="4">Donderdag</font></td>
					<td width="16%" colspan="10"><font size="4">Vrijdag</font></td>
				</tr>
		
				<tr align="center">
					<td width="12%" height="8">
					<td width="1.5%">1</td><td width="1.5%">2</td><td width="1.5%">3</td><td width="1.5%">4</td><td width="1.5%">5</td>
					<td width="1.5%">6</td><td width="1.5%">7</td><td width="1.5%">8</td><td width="1.5%">9</td><td width="1.5%"></td>
			
					<td width="1.5%">1</td><td width="1.5%">2</td><td width="1.5%">3</td><td width="1.5%">4</td><td width="1.5%">5</td>
					<td width="1.5%">6</td><td width="1.5%">7</td><td width="1.5%">8</td><td width="1.5%">9</td><td width="1.5%"></td>
				
					<td width="1.5%">1</td><td width="1.5%">2</td><td width="1.5%">3</td><td width="1.5%">4</td><td width="1.5%">5</td>
					<td width="1.5%">6</td><td width="1.5%">7</td><td width="1.5%">8</td><td width="1.5%">9</td><td width="3%"></td>
				
					<td width="1.5%">1</td><td width="1.5%">2</td><td width="1.5%">3</td><td width="1.5%">4</td><td width="1.5%">5</td>
					<td width="1.5%">6</td><td width="1.5%">7</td><td width="1.5%">8</td><td width="1.5%">9</td><td width="3%"></td>
				
					<td width="1.5%">1</td><td width="1.5%">2</td><td width="1.5%">3</td><td width="1.5%">4</td><td width="1.5%">5</td>
					<td width="1.5%">6</td><td width="1.5%">7</td><td width="1.5%">8</td><td width="1.5%">9</td><td width="3%"></td>
				</tr>
				<tr><td colspan="50"><hr></td></tr>	
				
				<?php 
				
				$lln=0;
				$regel="";
				$madag=1;
				$naamleerling = $leerlingen[$lln][1];
				
				for ($lln=0; $lln<$aantalleerlingen; $lln++) { // alle lessen van leerlingen aflopen
						
						if ($madag==1) {  //  is eerste les op maandag op het eerste uur?
							 $verschil = $leerlingen[$lln][4];
							 $madag=0;
							 if ($verschil > 0) {
					 		 		for ($s=1; $s<$verschil; $s++) {  // lege cellen vullen
					 						$regel = $regel . "<td></td>";
									}
							 }
						}
						
						$doc="-";
						
						$v1 = $leerlingen[$lln][6];
						$v2 = $leerlingen[$lln][8];
						$v3 = $leerlingen[$lln][10];
						
						//$doc=$v1;
						
						if (($v1=="dtu" or $v1=="Idl" or $v1=="") AND (!($v2=="") or !($v3==""))) { //  geen ingeroosterde les
							 if (!($v3=="" )) { 
							 		$doc = "<u>" . $v3 . "</u>";
							 }
							 if (!($v2=="" )) {
							 		$doc = "<u>" . $v2 . "</u>";
							 }
							 if (!($v2=="") AND !($v3=="")) {
							 		$doc = "<b>" . $doc . "</b>";
							 }
						}else{
							 $doc=$v1;
							 if (!($v2=="") or !($v3=="")) {$doc = "<b>" . $doc . "</b>";}
						}
												
						$regel = $regel . "<td>" . $doc . "</td>";
							 
						//  bepalen aantal stappen volgende les
						$lesnr1 = ($leerlingen[$lln][3])*10 + $leerlingen[$lln][4];
						$lesnr2 = ($leerlingen[$lln+1][3])*10 + $leerlingen[$lln+1][4];
						
						// echo $lesnr2-$lesnr1;
						
						$verschil = $lesnr2-$lesnr1;
						
						switch ($verschil)
						
						{
   					 	 case $verschil>1:
            	 				for ($s=1; $s<$verschil; $s++) {  // lege cellen vullen
							 		 				$regel = $regel . "<td></td>";
							 				}
           						break;
    					 
							 case $verschil<0:
           		 				echo "<tr align=\"center\"><td align=\"left\">" . $naamleerling . "</td>\n" . $regel . "\n</tr>";
						 	 				$regel="";
						 	 				$naamleerling = $leerlingen[$lln+1][1];
											$madag=1;
           						break;
    					 default:
           	
						} 
				}	
				echo "<tr height=\"25\" valign=\"bottom\"><td colspan=\"30\">* doc ingeroosterd vak | <u>doc</u> extra vrije ruimte vak | <b>doc</b> gelijktijdig met extra vrije ruimte vak | <u><b>doc</b></u> met overeenkomst vastgelegde ruil</td></tr>";
				
				?>
					
				
			</font>
					
		</table>
				
		</fieldset>	
	</div>	
		
</body>
</html>

