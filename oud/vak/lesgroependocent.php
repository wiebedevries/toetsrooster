<?php>
header ("P3P:CP=\"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM\"");
session_start(); 

function cleanup($string="") 
  { 
	$string = strip_tags($string); 
	$string = htmlspecialchars($string); 
	if(!get_magic_quotes_gpc()) { 
	  $string = addslashes($string); 
	} 
	return $string; 
} 

// controleren of pagina correct is aangeroepen.

include("inc_connect.php"); 

if (!isset($_SESSION['code'])) { 
	$tekst = "<font face=\"verdana\" size=\"4\">Je hebt geen geldige roostercode opgegeven.<br>Probeer opnieuw: 
	<a href=\"index.htm\"\" onmouseover=\"window.status='';return true\">Opnieuw inloggen</a></font><br>";
	die($tekst); 
}else{
	
	$klas = cleanup($_POST['klas']);
	$code 			= $_SESSION['code'];
	$icode 			= $_SESSION['icode'];
	$admin      = $_SESSION['admin'];
	$wachtwoord = $_SESSION['wachtwoord'];
	
	if (!isset($_POST['cdocent'])) {
		 $cdocent = $code;
	}else{
		 $cdocent = $_POST['cdocent'];
	}
	
	$query = "SELECT * FROM leerlingen WHERE doc1 = '$cdocent' OR doc2 = '$cdocent' OR doc3 = '$cdocent' ORDER by klas,naam,doc3,doc2,doc1";
	$result = mysql_query($query) or die ("FOUT: " . mysql_error());
	$aantalleerlingen = mysql_num_rows($result);
	$leerlingen = array();
	while($leerlingen[] = mysql_fetch_array($result)); // data uit tabel:weken in $week[index][velden]
	
	$query = "SELECT DISTINCT doc1 FROM leerlingen ORDER by doc1";
	$result = mysql_query($query) or die ("FOUT: " . mysql_error());
	$aantaldocenten = mysql_num_rows($result);
	$docenten = array();
	while($docenten[] = mysql_fetch_array($result)); // data uit tabel:weken in $week[index][velden]
	
	$query = "SELECT * FROM users WHERE code = '$code' AND wachtwoord = '$wachtwoord'"; 
	$result = mysql_query($query) or die("FOUT : " . mysql_error()); 
	while($docentgegevens[] = mysql_fetch_array($result)); // docentinformatie
	$docent = $docentgegevens[0][2];
	if (mysql_num_rows($result) > 0){ // er bestaat een docent met code
		
		date_default_timezone_set('Europe/Amsterdam');
		setlocale(LC_ALL, 'nl_NL');

		$vandaag = strftime("%Y-%m-%d", mktime(date("j F Y")));
		$uvandaag = strftime("%A %e %B %Y", mktime(date("j F Y")));
		$hweek		 = intval(strftime("%W", strtotime($vandaag)));
		$_SESSION['week'] 		  = $week;
  	$_SESSION['vandaag'] 	  = $vandaag;
		
	
	}else{
		
		echo "De door u ingevoerde code komt niet voor!";
		header("Location: login.php");
		exit();
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="nl" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo "Lesgroepen van&nbsp;" . ucfirst($cdocent) ?></title>

<script language="JavaScript">
		<!-- Begin

		function myprint() {
		window.focus();
		window.print();
		}
		//  End -->
</script>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<style type="text/css"> 

a.tooltip{
		position:relative;
		padding:0 0.1em 0 0.1em}
a.tooltip x{
		position:relative;
		text-decoration:underline;
		padding:0 0.1em 0 0.1em}
a.tooltip span{
		padding:0.5em;
		display:none}
a.tooltip:hover span{
		text-decoration:none;
		display:block;
		position:absolute;
		top:-3.5em;
		left:-6.0em;
		width:13em;
		border:1px solid #d7dbdf;
		background:#f7fbff;
		color:#000000;
		font-size: 0.8em;
		text-align:center}

body{
		margin: 0;
		font-family: arial narrow, sans-serif;
		font-size: 0.75em;
		}

h1 {   
page-break-after: avoid;   
}
				
body1{
margin: 0;
font-family: Verdana, sans-serif;
font-size: 0.8em;
}

div#header{
position: fixed;
font-family: arial, sans-serif;
top: 0;
left: 0;
width: 100%;
height: 100px;
background-color: #e7ebef;
}
 
div#content{
padding: 100px 20px 0 20px;
padding-left: 2%;
height: 40px;
background-color: #e7ebef;
}

div#content1{
  padding: 0px 20px 0 20px;
	padding-left: 2%;
	width: 96%;
	background-color: #e7ebef;
}

</style>

<style type="text/css" media="print"> 
		
		#content{
		display: none;
		}
		
		#header{
		display: none;
		}
		
		div.page { 
		writing-mode: tb-rl; 
		width: 100%;
		height: 100%; 
		margin: 10% 0%;
		size: landscape;
		}
		
		#footer{
  	display:none;
}

</style>

</head>

<body bgcolor="#e7ebef">
 
<div id="header">

	<?php
	// buttons opgeven
	$actief = 5;
	$button[1][1] = "Mijn toetsen" 			 ; $button[1][2] = "overzicht.php";
	$button[2][1] = "Toetsen opgeven" 	 ; $button[2][2] = "opgeventoetsen.php";
	$button[3][1] = "Overzicht per klas" ; $button[3][2] = "toetsroosterklas.php";
	$button[4][1] = "Mijn toetsrooster"  ; $button[4][2] = "toetsroosterdocent.php";
	$button[5][1] = "Lesgroepen"  			 ; $button[5][2] = "lesgroependocent.php";
	
	$aantalbuttons = 4;
 	?>
	
	<table border="0" width="96%" cellpadding="1" align="center">
		<tr>
			<td align="left" width="70%" valign="middle" height="40">
					
					<?php
					 if ($admin==0) { // geen admin
					 		echo "<font size=\"5\">" . $docent . "</font>";
					 }else{
					 		echo "<font size=\"5\" color=\"#ff0000\"><b>" . ucfirst($code) . 
									 "</b></font><font size=\"4\" color=\"#ff0000\"><i>&nbsp;&nbsp;(" . $docent . ")&nbsp;</i>";
					 }
					?>
					
					<font size="1"><i><?php echo $uvandaag?></i></font></td>
					 
			<td align="right" width="30%"><font size="5"><?php echo $button[$actief][1]?></font></td>
		</tr>
		
	</table>

	<table border="0" width="96%" cellpadding="1" align="center">
				
		<tr>
			<?php
			
			for ($i=1; $i<$aantalbuttons+1; $i++) {
					echo "<td align=\"center\" width=\"90\">";
					echo "<form style=\"margin:0;padding:0;text-decoration: none;color: navy;\" method=\"post\" ";
					echo "action=\" " . $button[$i][2] . "\" target=\"_self\" onmouseover=\"window.status='';return true\">";
					if ($actief==$i) {
						 echo "<input type=\"submit\" value=\" " . $button[$i][1] . "\" style=\"color: #bbbbbb\" ></form></td>";
					}else{
						 echo "<input type=\"submit\" value=\" " . $button[$i][1] . "\"></form></td>";
					}
			}
			echo "<td></td>";
			?>
			
			<td width="90" align="center">
					<input type =button name="cmdPrint" value = "Afdrukken"
					onClick="myprint()" style="color: navy;" style="text-decoration: none;">
			</td>
				
			<td width="90"></td>
				
			<td width="90" align="right">
					<form style="margin:0;padding:0;text-decoration: none;color: navy;" method="post"
							 onmouseover="window.status='';return true"
							 action="lesgroepenmentor.php" target="_self"><input type="submit" value="M">
					</form>
			</td>	
					
			<td width="90" align="right">
					<form style="margin:0;padding:0;text-decoration: none;color: navy;" method="post"
							 onmouseover="window.status='';return true"
							 action="uitloggen.php" target="_self"><input type="submit" value="Uitloggen">
					</form>
			</td>
					
		</tr>
		<tr><td colspan="10"><hr></td></tr>	
	</table>
</div>

<div id="content">

			<form method="post" action="lesgroependocent.php "alt="">
				<table width="96%"> 
					<tr valign="middle">
						<td width="1%"></td>
						<td valign="middle" height="15" width="10%" align="left" >
							<select name="cdocent" size="1" onchange="this.form.submit()">
								<?php
								echo "<option>" . $cdocent ."</option>";
								for ($i=1; $i<$aantaldocenten; $i++) {
										if ($docenten[$i][0] == "0" or $docenten[$i][0] == "Idl" or 
											 	$docenten[$i][0] == "dtu" or $docenten[$i][0] == $cdocent) {
										}else{
											  echo "<option>" . $docenten[$i][0] . "</option><br>";
										}
								}
							?>	
							</select>				
						</td>
						
						<td valign="middle" height="15" width="10%">
							
						</td>
						
						
						<td width="20%"></td>
						
												
						
					</tr>
				</table>
			</form>
		<br>
</div>	

<div id="content1">
		
		 
		 		<?php
				
				$naamdag[1] = "Maandag";
				$naamdag[2] = "Dinsdag";
				$naamdag[3] = "Woensdag";
				$naamdag[4] = "Donderdag";
				$naamdag[5] = "Vrijdag";
				$aantalgp   = 0;
				
				//echo "<fieldset><table width = \"96%\"><tr valign=\"top\" width=\"96%\" height=\"960\">\n";
				for ($dag=1; $dag<6; $dag++) { // alle dagen aflopen
						
						for ($uur=1; $uur<10; $uur++) { // alle lesuren aflopen
							$lesgroep = "";
							$aantalln = 0;
							$aantallnniet = 0;
							$niet = 0;  // registreert niet te volgen vak voor tellen leerlingen
							$hklas    = "";
							$x				= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
							$av       = 0;
							
							for ($i=0; $i<$aantalleerlingen; $i++) {	
									if ($leerlingen[$i][3]==$dag AND $leerlingen[$i][4]==$uur) { // juiste dag en juiste lesuur
							 				If (!($hklas == $leerlingen[$i][2])) { // nieuwe klas
												 $hklas = $leerlingen[$i][2];
												 $lesgroep = $lesgroep . "<br>" . $x . "<font size=\"2\"><b><u>" . $hklas . "</b></u>&nbsp;&nbsp;&nbsp;<b>" . $leerlingen[$i][5] . "</b></font><br><br>\n";
											}
											
											$vv=""; // voorvoegsel voor naam met info over vak (r = ingeroosterd, v = vrije ruimte
											
											if (($leerlingen[$i][6])==$cdocent) {
												 $vv = "r"; // vak 1 ingerooster vak voor deze docent
												 $av = $av + 1;
											}else{
												 if (!($leerlingen[$i][6]=="") And $leerlingen[$i][8]==$cdocent OR
												 		 !($leerlingen[$i][6]=="") And $leerlingen[$i][10]==$cdocent){
												 		$vv = "<s>v</s>"; // vak van deze docent is vrije ruimte vak
												 		$av = $av + 1;
														$niet = 1;
												 }else{
												 		if ($leerlingen[$i][6]=="" And $leerlingen[$i][8]==$cdocent OR
												 			 $leerlingen[$i][6]=="" And $leerlingen[$i][10]==$cdocent){
												 			 $vv = "v"; // vak van deze docent is vrije ruimte vak
												 			 $av = $av + 1;
												 		}
												 }
											}	 
																						
											$vv = "&nbsp;<font size=\"2\">" . $vv . "&nbsp;&nbsp;</font>";
											
											$lesgroep = $lesgroep . "<input type=\"checkbox\" name=\"leerling\" value=\"" . 
											$i . "\">" . $vv . $leerlingen[$i][1] . "<br>\n";
											if ($niet==0) { 
												 $aantalln = $aantalln + 1;
											}else{
												 $aantallnniet = $aantallnniet + 1;
												 $niet = 0;
											}
									}
								}
								if ($aantalln>0) { // als er leerlingen in deze lesgroep zitten
									 if ($aantalgp ==0) {echo "<fieldset><table width = \"96%\" height=\"940\"><tr valign=\"top\" width=\"96%\">\n";}
									 		if ($aantallnniet != 0) {
												 echo "<td width=\"24%\"><font size=\"3\">&nbsp;&nbsp;&nbsp;&nbsp;" . 
												 $naamdag[$dag] . "&nbsp;&nbsp;" . $uur . "</font><br>\n" . $lesgroep . 
												 "<br><i>" . $x . "aantal: " . $aantalln . " (". $aantallnniet . ") </i></td>\n";
											}else{
												 echo "<td width=\"24%\"><font size=\"3\">&nbsp;&nbsp;&nbsp;&nbsp;" . 
												 $naamdag[$dag] . "&nbsp;&nbsp;" . $uur . "</font><br>\n" . $lesgroep . 
												 "<br><i>" . $x . "aantal: " . $aantalln . "</i></td>\n";
											}
									 $aantalgp = $aantalgp + 1;
								}
								if ($aantalgp == 4 ) { //
						 			 echo "</tr></table><i>* r ingeroosterd vak, v te volgen vrije ruimte vak, <s>v</s> niet te volgen extra vrije ruimte vak i.v.m. andere les, o met overeenkomst vastgelegde ruil</i></fieldset>\n";
									 echo "<br><h1 id=\"h1\"></h1>";

									 $aantalgp = 0;
						 		}
						}
				}
				if (!($aantalgp == 0 )) {echo "</tr></table><i>* r ingeroosterd vak, v te volgen vrije ruimte vak, <s>v</s> niet te volgen vrije ruimte vak i.v.m. andere les, o met overeenkomst vastgelegde ruil</i></fieldset><br>\n";}
			?>
		
</div>	
		
</body>
</html>

