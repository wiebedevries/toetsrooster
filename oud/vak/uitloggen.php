<?php 
session_start(); 
session_unset(); // alle variabelen vrijgeven 
session_destroy(); // sessie afsluiten 
?> 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="nl" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Toetsen - Lyceum onderbouw</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style type="text/css"> 
body{
margin: 0;
font-family: Verdana, sans-serif;
font-size: 0.8em;
}
 
div#header{
position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100px;
background-color: #e7ebef;
}
 
div#content{
padding: 110px 20px 0 20px;
background-color: #e7ebef;
}
</style>
</head>

<body bgcolor="#e7ebef">
 
<div id="header">
	<table border="0" width="95%" cellpadding="1">
				
		 		<tr align="left" valign="middle">
						<td width="5%" rowspan="2"></td>
						<td width="15%" align="left" height="30">
					 		 <form style="margin:0;padding:0;text-decoration: none;color: navy;" method="get"
					 		 action="index.htm"  target="_self"><input type="submit"
							 value="Inloggen">
							 </form></td>
					 <td width="15%" align="middle"></td>
					 <td width="15%" align="right"></td>
					 <td width="45%" align="right"></td>
					 <td width="5%"></td>
				</tr>
				<tr>
					 <td height="50" align="left" colspan="3"><font size="5">Je bent uitgelogd!</font></td>
				</tr>
	</table>
</div>
 
<div id="content">
	<hr>
<br>
</div>
</body>
</html>

