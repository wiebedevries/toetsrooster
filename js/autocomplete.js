
/** Autocomplete **/
(function() {
    var ANIMATION_TIME = 100;
    var TAB_CODE = 9;
    var SHIFT_CODE = 16;

    window.ac = function() {
        this.json = null;
        var element = null;
        var autoCompleteElement;
        var _this = this;

        this.initAjax  = function(ajax) {
            ajax.done(function(response) {
                console.log(response);
                _this.json = response;
            });
        };

        this.initJson = function(data) {
            _this.json = data;
        };

        this.init = function(el, ael) {
            element = el;
            autoCompleteElement = ael;

            $(element).prop("autocomplete", "off");
            $(element).on("blur", _this.hideSuggestionBox());

            $(element).on("keyup", function(e) {
                if (e.keyCode == TAB_CODE || e.keyCode == SHIFT_CODE) {
                    _this.hideSuggestionBox();
                    return;
                }

                if ($(element).val().trim() == "") {
                    _this.hideSuggestionBox();
                    return;
                }

                var suggestionHtml = _this.getSuggestionHtml();
                if (suggestionHtml != "") {
                    $(autoCompleteElement).html(_this.getSuggestionHtml());
                    _this.addEventListeners(autoCompleteElement);
                    _this.showSuggestionBox();
                }
            });

            $(element).on("keydown", function(e) {
                if (e.keyCode == TAB_CODE) {
                    var matches =_this.getSuggestions();
                    _this.hideSuggestionBox();

                    if (matches.length == 1 && $(this).val() != matches[0]) {
                        e.preventDefault();
                        $(element).val(matches[0].value);
                    }
                }
            });
        };

        this.getSuggestions = function() {
            var value = $(element).val().trim();
            var matches = [];

            if (value == "" || _this.json == null) {
                return [];
            }

            for (var i=0; i<_this.json.length; i++) {
                if (_this.json[i].value.indexOf(value) != -1) {
                    matches.push(_this.json[i]);
                }
            }

            return matches;
        };

        this.getSuggestionHtml = function() {
            var html = "<ul class='list-group'>";
            var value = $(element).val().trim();
            var matches = _this.getSuggestions();

            for(var m in matches) {
                var match = matches[m].value;
                var i = match.indexOf(value);
                html += "<li class='list-group-item' data-value='" + match + "'>" +
                    match.substr(0, i) + "<b class='mark'>" + match.substr(i, value.length) + "</b>" + match.substr(i + value.length, match.length - (i + value.length))
                "</li>";
            }


            if (matches.length == 0) {
                return "";
            } else {
                html += "</ul>";
                return html;
            }
        };

        this.addEventListeners = function(el) {
            $(el + " .list-group-item").on("mousedown", function(e) {
                e.preventDefault();
                $(element).val($(this).data("value"));

                _this.hideSuggestionBox();
            });
        };

        this.showSuggestionBox = function() {
            $(autoCompleteElement).slideDown(ANIMATION_TIME);
            $(element).css(
                {
                    "borderBottomLeftRadius" : 0,
                    "borderBottomRightRadius" : 0,
                    "borderBottom" : 0
                }
            );
        };

        this.hideSuggestionBox = function() {
            $(autoCompleteElement).slideUp(0);
            $(element).removeAttr("style");
        }
    };
})();