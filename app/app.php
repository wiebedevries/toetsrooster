<?php
// include required classes
require "class/SQL.class.php";
require "class/Rooster.class.php";
require "class/Toets.class.php";
require "class/Klas.class.php";
require "class/Foward.class.php";
require "class/Session.class.php";
require "class/Request.class.php";
require "class/Html.class.php";

require "Cleanup.function.php";

// start session
Session::start();

// init app
require "app/init.php";