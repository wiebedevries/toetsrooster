<?= Html::header("Pagina niet gevonden", false); ?>

<div class="content">
    <p>De pagina <?= $page; ?> is niet gevonden.</p>
</div>

<?= Html::footer(); ?>
