<?php
$klas = Request::get("argument", false);
$klas = Request::post("klas", $klas);

$klassen = Klas::alle();
$klas_namen = [];
foreach ($klassen as $k) {
    $klas_namen[] = $k["klas"];
}

// id ophalen van huidige week
$huidige_week = SQL::select("SELECT * FROM weken WHERE week = $week");
$wid = $huidige_week[0]["id"];
$weken = SQL::select("SELECT * FROM weken WHERE id >= $wid ORDER BY id");

$selected_week = $weken[0]["week"];
$selected_week_id = $weken[0]["id"];

$disable_week = true;
?>

<?= Html::header("Toetsrooster van " . ucfirst($klas), false); ?>
    <div id="header">
        <div class="header-info">
            <h3 class="page-info">
                Toetsrooster | <?= ($klas != "") ? $klas . " | " : ""; ?> week <?= $week; ?>
            </h3>
        </div>
    </div>
    <?php if ($klas == false || !in_array($klas, $klas_namen)) : ?>
        <?php if ($klas != "") : ?>
            <div class="content">
                <h2>Klas '<?=$klas; ?>' niet gevonden.</h2>
            </div>
        <?php endif; ?>

        <div class="content">
            <div class="input-group left">
                <label for="klas">Klas</label>
                <select name="klas" id="klas">
                    <option>Kies klas</option>
                    <?php foreach ($klassen as $klas_option) : ?>
                        <option <?= $klas_option["klas"] == $klas ? "selected" : ""; ?>>
                            <?= ucfirst($klas_option["klas"]); ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
    <?php else :  ?>
        <?php require "rooster.page.php"; ?>

        <div class="content">
            <p><b><i>Let op: Vanaf week <?= $selected_week + 3 ?> kan het toetsrooster nog worden gewijzigd.</i></b></p>
        </div>
    <?php endif; ?>
<?= Html::startFooter(); ?>
    <script>
        $("#klas").on("change", function() {
            var klas = $("#klas").val();

            window.location.href = "/klas/" + klas + "/";
        });
    </script>
<?= Html::endFooter(); ?>
