<?php

if (Request::post("h") != "") {
    $headers = Request::post("h");
    $rows = Session::get("rooster_rows");

    SQL::delete("rooster", "week = 42");

    foreach ($rows as $row) {
        $columns = explode(";", $row);
        $data = [];
        foreach ($headers as $hIndex => $hValue) {
            if ($hValue == "id") {
                continue;
            }

            if (isset($columns[$hIndex])) {
                $data[$hValue] = trim($columns[$hIndex]);
            }
        }

        if (!empty($data)) {
            $data["klas"] = substr($data["klas"], 0, 4);
            SQL::insert("rooster", $data);
        }
    }

    Foward::to("/settings/");
}

$file = isset($_FILES["fileToUpload"]) ? $_FILES["fileToUpload"] : null;

if ($file == null) {
    Foward::to("/settings/");
}

$content = file_get_contents($file["tmp_name"]);
$rows = explode("\n", $content);
$count_rows = count($rows);

$header = array_shift($rows);
$header_columns = explode(";", $header);

Session::set("rooster_rows", $rows);
?>

<?= Html::header("Upload nieuw rooster"); ?>

<form method="POST">
    <div class="content">
        <h1>Instellingen <small>Upload rooster</small></h1>

            <div class="row">
                <div class="col-md-12">
                    <div class="btn-group left">
                        <button class="btn" type="submit">Bevestigen</button>
                    </div>
                </div>
            </div>
    </div>

    <div class="content">
        <table class="table table-striped">
            <tr>
                <?php foreach ($header_columns as $h) : ?>
                    <th>
                        <?= $h; ?>
                        <input type="hidden" name="h[]" value="<?= trim($h); ?>"/>
                    </th>
                <?php endforeach; ?>
            </tr>
            <?php for ($r=0; $r<$count_rows - 1; $r++) : ?>
                <tr>
                    <?php foreach (explode(";", $rows[$r]) as $column) : ?>
                        <td>
                            <?= $column; ?>
                        </td>
                    <?php endforeach; ?>
                </tr>
            <?php endfor; ?>
        </table>
    </div>
</form>


<?= Html::footer(); ?>