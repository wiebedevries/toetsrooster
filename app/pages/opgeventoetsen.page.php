<?php
// unieke klassen van betreffende docent ophalen
$klassen = Klas::getKlassen($code);
$aantalklassen = count($klassen);

$klas = isset($klassen[0]["klas"]) ? $klassen[0]["klas"] : "";
$klas = Session::get("klas") != "" ? Session::get("klas") : $klas;
$klas = Request::post("klas", $klas);
$kl = $klas;

Session::set("klas", $klas);

// unieke vakken van betreffende docent ophalen
$vakken = [];
$vakken_array = SQL::select("SELECT DISTINCT vak FROM rooster WHERE docent = '$code' ORDER BY vak");
foreach ($vakken_array as $vak) {
    $vakken[] = $vak["vak"];
}

$toetsen = Toets::toetsen($code);

// ophalen van geldige weken
$date = date("Y-m-d");
$weken = [];
if ($admin) {
    $huidige_week = SQL::select("SELECT * FROM weken WHERE week = $week");
    $id = $huidige_week[0]["id"];
    $weken = SQL::select("SELECT * FROM weken WHERE id >= $id");
} else {
    $weken = SQL::select("SELECT * FROM weken WHERE '$date' < voor");
}

$aantalweken = count($weken);

$selected_week = Request::post("cweek", $weken[0]["week"]);
$selected_week_id = 0;

foreach ($weken as $w) {
    if($w["week"] == $selected_week) {
        $selected_week_id = $w["id"];
    }
}

// make sure that toetsen can be added
$can_edit = true;
?>

<?= Html::header("Opgeven toetsen voor&nbsp;" . ucfirst($klas)); ?>

<?php require "rooster.page.php"; ?>

<?= Html::startFooter(); ?>
    <script>
        $(".new-toets").each(function(index, value) {
            $(this).on("change", function() {
                var vakken = $(value).data("vakken");
                var data = {
                    code : $(value).data("code"),
                    klas : $(value).data("klas"),
                    vak : $(value).data("vak"),
                    gewicht : $(value).val(),
                    les : $(value).data("les"),
                    week : $(value).data("week"),
                    weekid : $(value).data("weekid")
                };

                toetsen.add(data);
                var parent = $(this).parent();
                parent.addClass("toets");
                parent.html(vakken.replace(data.vak, "<b>" + data.vak + "-" + data.gewicht + "</b>"));
            });
        });
    </script>
<?= Html::endFooter(); ?>

