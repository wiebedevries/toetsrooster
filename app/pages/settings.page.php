
<?= Html::header("Toetsrooster beheer"); ?>


<div class="content">
    <h1>Instellingen <small>gebruikers</small></h1>

        <div class="content">
            <div class="row">
                <div class="input-group col-lg-8">
                    <div class="left md-fixed">
                        <label for="user" class="control-label left">Zoek gebruiker</label>
                    </div>

                    <div class="input col-xs-9">
                        <input type="text" id="users"/>
                        <div id="users-suggestion" class="autocomplete-suggestion"></div>
                    </div>

                    <div class="left xs-fixed">
                        <button class="btn" id="searchUser">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>

    <div id="usersContent"></div>
</div>

<div class="content">
    <h1>Instellingen <small>rooster</small></h1>

    <div class="content">
        <form action="/upload_rooster/" method="POST" enctype="multipart/form-data">
            <div class="row">
                <div class="input-group col-lg-8">
                    <div class="left md-fixed">
                        <label for="fileToUpload" class="btn left">
                            Kies bestand
                        </label>
                        <input type="file" name="fileToUpload" id="fileToUpload" style="display: none">
                    </div>

                    <div class="input col-xs-6">
                        <input type="text" disabled="disabled" id="fileName">
                    </div>

                    <div class="left md-fixed">
                        <button type="submit" class="btn left">Uploaden</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<?= Html::startFooter(); ?>
    <script>
        var users = [];

        $(document).ready(function() {
            var acUsers = new ac();
            $.get("/handle/get_users/", function(json) {
                users = json;

                acUsers.init("#users", "#users-suggestion");
                acUsers.initJson(users);
            });

            $("#searchUser").on("click", function() {
                $.post("/handle/get_user/", { "naam" : $("#users").val() }, function(html) {
                    $("#usersContent").html(html);

                    $(".btn-save-user").each(function(index, value) {
                        $(this).on("click", function() {


                            var user_id = $(this).data("user-id");
                            var admin = $("#user-" + user_id + "-admin").val();
                            var aangemeld = $("#user-" + user_id + "-nieuw").prop("checked") ? 0 : 1;

                            var data = {"id" : user_id, "admin" : admin, "aangemeld" : aangemeld};
                            handle.save("/handle/save_user/", data);
                        });
                    });
                });
            });

            $("#resetBtn").on("click", function() {
                $("#usersContent").html("");
            });

            $("#fileToUpload").on("change", function() {
                $("#fileName").val($(this).val());
            });
        });
    </script>
<?= Html::endFooter(); ?>
