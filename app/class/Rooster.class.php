<?php

class Rooster
{
    public static $days = ["Maandag", "Dinsdag", "Woensdag", "Donderdag", "Vrijdag"];
    public static $day_keys = ["ma", "di", "wo", "do", "vr"];

    public static function getByKlas($week = 42, $klas)
    {
        return SQL::select(
    "
              SELECT * 
              FROM rooster 
              WHERE week = '$week' 
                AND klas = '$klas' 
            ORDER BY dag
          "
        );
    }

    public static function getByDocent($week = 42, $docent)
    {
        return SQL::select(
            "
                SELECT *
                FROM rooster
                WHERE week = '$week'
                  AND docent = '$docent'
                ORDER BY dag
            "
        );
    }

    public static function sortVolledigRooster($rooster, $data, $weken, $toetsen)
    {
        $toetsen_per_week_les = Toets::sortToetsWeekLes($toetsen);
        $volledig_rooster = [];

        for ($w=0; $w<$weken; $w++) {
            $week = $data[$w]["week"];
            $les_van_week = [];

            foreach ($rooster as $r) {
                $day = (int) $r["dag"] - 1;
                $uur = (int) $r["uur"] - 1;
                $vak = $r["vak"];
                $klas = trim($r["klas"]);
                $day_key = self::$day_keys[$day];
                $les_key = $vak . "-" . $klas;

                // leerlingen zijn les- of toetsvrij deze dag, dus deze mag overgeslagen worden.
                if ($data[$w][$day_key] == 2 || $data[$w][$day_key]) {
                    continue;
                }

                $les_van_week[$les_key] = (isset($les_van_week[$les_key])) ? $les_van_week[$les_key] + 1 : 1;

                if (isset($volledig_rooster[$w][$day][$uur]["rooster"])) {
                    $volledig_rooster[$w][$day][$uur]["rooster"]["vak"] .= "<br/>" . $vak;
                } else {
                    $volledig_rooster[$w][$day][$uur]["rooster"] = $r;
                }

                $l = $les_van_week[$les_key];
                if (isset($toetsen_per_week_les[$week][$vak][$l]) && $toetsen_per_week_les[$week][$vak][$l]["klas"] == $klas) {
                    $volledig_rooster[$w][$day][$uur]["toets"] = $toetsen_per_week_les[$week][$vak][$l];
                }
            }
        }

        return $volledig_rooster;
    }
}