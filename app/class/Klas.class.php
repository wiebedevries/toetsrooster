<?php

class Klas
{
    /**
     * @param $code
     * @return string
     */
    public static function getMentorKlas($code)
    {
        $klassen = SQL::select(
        "
            SELECT DISTINCT klas 
            FROM rooster 
            WHERE docent = '$code'
                AND vak = 'mtu'
            "
        );

        if (count($klassen) > 0 ) {
            return $klassen[0]["klas"];
        } else {
            return "";
        }
    }

    public static function getKlassen($code)
    {
        return SQL::select(
            "
                SELECT DISTINCT klas
                FROM rooster
                WHERE docent = '$code'
                ORDER BY klas
            "
        );
    }

    public static function alle()
    {
        return SQL::select(
            "
                SELECT DISTINCT klas
                FROM rooster
                ORDER BY klas
            "
        );
    }
}