<?php


class Request
{
    public static function get($key, $optional = "")
    {
        return (isset($_GET[$key])) ? $_GET[$key] : $optional;
    }

    public static function post($key, $optional = "")
    {
        return (isset($_POST[$key])) ? $_POST[$key] : $optional;
    }
}