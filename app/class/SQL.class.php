<?php

class SQL
{
    public static function connect()
    {
        try {
            $conn = new PDO("mysql:host=" . DB_HOST .";dbname=" . DB_NAME . ";charset=utf8", DB_USER, DB_PASS);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return $conn;
        }
        catch(PDOException $e)
        {
            echo "Connection failed: " . $e->getMessage();
        }

        return null;
    }

    private static function run($query, $arguments = [])
    {
        $conn = self::connect();
        $stmt = $conn->prepare($query);
        return $stmt->execute();
    }

    public static function select($query, $arguments = [])
    {
        $conn = self::connect();
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        return $stmt->fetchAll();
    }

    public static function update($table, $data, $where)
    {
        $query = "UPDATE " . $table . " SET ";
        foreach ($data as $key => $value) {
            $query .= $key . "='" . $value . "',";
        }
        $query = substr($query, 0, strlen($query) - 1);

        if ($where != "") {
            $query .= " WHERE (" . $where . ")";
        }

        $conn = self::connect();
        $stmt = $conn->prepare($query);

        return $stmt->execute();
    }

    public static function delete($table, $where) {
        self::run("DELETE FROM $table WHERE " . $where);
    }

    public static function insert($table, $data)
    {
        $columns = implode(",", array_keys($data));
        $values = "";

        foreach ($data as $columnKey => $columnValue) {
            $values .= "'" . $columnValue . "',";
        }

        $values = substr($values, 0, strlen($values) - 1);
        $query = "INSERT INTO $table ($columns) VALUES ($values)";

        self::run("INSERT INTO $table ($columns) VALUES ($values)");
    }
}