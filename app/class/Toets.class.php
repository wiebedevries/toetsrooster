<?php

class Toets
{
    public static function toetsen($code)
    {
        return SQL::select(
        "
                SELECT * 
                FROM toetsopgaven 
                WHERE code = '$code'
                  AND isdeleted = 0
                ORDER BY klas, weekid
              "
        );
    }

    public static function toetsenPerWeek($week_from, $week_to, $klas = "", $code = "")
    {
        return SQL::select(
            "
              SELECT * 
              FROM toetsopgaven 
              WHERE weekid >= '$week_from' 
                AND weekid <= '$week_to' 
                " . ($klas != "" ? "AND klas = '$klas'" : "") . "
                " . ($code != "" ? "AND code = '$code'" : "") . "
                AND isdeleted = 0
              ORDER BY week
          "
        );
    }

    public static function toetsenPerKlas($code)
    {
        $toetsen = self::toetsen($code);
        $toetsen_per_klas = [];

        foreach ($toetsen as $toets) {
            $toetsen_per_klas[$toets["klas"]][] = $toets;
        }

        return $toetsen_per_klas;
    }

    public static function sortToetsWeekLes($toetsen)
    {
        $toetsen_per_week_les = [];

        foreach ($toetsen as $toets) {
            $week = (int) $toets["week"];
            $les = (int) $toets["les"];
            $vak = $toets["vak"];

            $toetsen_per_week_les[$week][$vak][$les] = $toets;
        }

        return $toetsen_per_week_les;
    }
}