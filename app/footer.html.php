
    <?php if (DEV && $show_toolbar) : ?>
    <div id="footer">
        <div class="left auto-width">
            SERVER: <?= $_SERVER["HTTP_HOST"]; ?>
        </div>
        <div class="left auto-width">
            DB: <?= DB_HOST; ?>
        </div>
        <div class="left auto-width">
            TB: <?= DB_NAME; ?>
        </div>
    </div>
    <?php endif; ?>

    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="/js/script.js"></script>
    <script src="/js/autocomplete.js"></script>
</body>
</html>