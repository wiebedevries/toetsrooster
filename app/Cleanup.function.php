<?php

function cleanup($string="")
{
    $string = strip_tags($string);
    $string = htmlspecialchars($string);
    if(!get_magic_quotes_gpc()) {
        $string = addslashes($string);
    }
    return $string;
}