<?php

$naam = \Request::post("naam", "");
$found_users = SQL::select("SELECT * FROM users WHERE naam LIKE '$naam%'");

$rechten = [
    0 => "Standaar gebruiker",
    1 => "Toetsbeheerder",
    2 => "Administrator"
];

?>

<h1>Gevonden gebruikers</h1>
<div class="content">
    <table class="table table-striped table-border">
        <tr>
            <th>Naam</th>
            <th>Administrator</th>
            <th>Nieuw</th>
            <th></th>
        </tr>
        <?php foreach ($found_users as $user) : ?>
            <tr>
                <td><?= $user["naam"]; ?></td>
                <td class="small">
                    <select name="" id="user-<?= $user["id"]; ?>-admin">
                        <?php foreach ($rechten as $rightKey => $rightName) : ?>
                            <option <?= ($user["admin"] == $rightKey) ? "selected" : ""; ?> value="<?= $rightKey; ?>">
                                <?= $rightName; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </td>
                <td class="small">
                    <input type="checkbox" id="user-<?= $user["id"]; ?>-nieuw"  <?= ($user["aangemeld"] == 0) ? "checked" : ""; ?>/>
                </td>
                <td class="small">
                    <div class="btn-group right">
                        <a href="javascript:;"
                           class="btn btn-save-user left"
                           data-user-id="<?= $user["id"]; ?>">
                            <i class="glyphicon glyphicon-floppy-disk"></i>
                        </a>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>
