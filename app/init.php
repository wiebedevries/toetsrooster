<?php

// set default timezone
date_default_timezone_set('Europe/Amsterdam');
setlocale(LC_ALL, 'nl_NL');

// defining default variables
$icode = Session::get('icode');
$admin = Session::get('admin');
$code = Session::get('code');
$docent = Session::get("docent");

$vandaag = Session::get("vandaag");
$uvandaag = Session::get("uvandaag");

$user = Session::get("user");

// define constants
define("DOCENT", $docent);
define("CODE", $code);

if ($vandaag == null || $uvandaag == null) {
    $vandaag = strftime("%Y-%m-%d", strtotime(date("j F Y")));
    $uvandaag = strftime("%A %e %B %Y", strtotime(date("j F Y")));
    Session::set("vandaag", $vandaag);
    Session::set("uvandaag", $uvandaag);
}


$date = new DateTime();
$week = $date->format("W");

Session::set("week", $week);
