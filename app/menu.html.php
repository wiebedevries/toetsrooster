<div id="header">
    <?php
    $page = Session::get("page");
    $code = Session::get('code');
    $icode = Session::get("icode");
    $admin = Session::get('admin');
    $docent = Session::get("docent");
    $week = Session::get("week");
    $user = Session::get("user");

    $buttons = [
        "Mijn toetsen" => "/overzicht/",
        "Toetsen opgeven" => "/opgeventoetsen/",
        "Overzicht per klas" => "/toetsroosterklas/",
        "Mijn toetsrooster" => "/toetsroosterdocent/"
    ];
    ?>

    <div class="message-balloon"></div>

    <div class="header-info">
        <h3 class="page-info">
            Toetsrooster | Week <?= $week; ?>
        </h3>

        <div class="header-name">
            <?= ($code != $icode) ? "<b>" . ucfirst($code) . "</b><i>(" . $docent . ")</i>" : $docent; ?>
        </div>
    </div>

    <div class="menu-btn-group">
        <div class="btn-group left">
            <?php foreach ($buttons as $name => $link) : ?>
                <a class="btn left <?= strpos($link, $page) !== false ? "active" : ""; ?>" href="<?= $link; ?>"><?= $name; ?></a>
            <?php endforeach; ?>
        </div>

        <div class="btn-group right">
            <a href="/manual/" class="btn right">
                Help
            </a>
            <?php if ($admin == 2) : ?>
                <a href="/settings/" class="btn right">
                    <i class="glyphicon glyphicon-cog"></i>
                </a>
            <?php endif; ?>
            <a href="/uitloggen/" class="btn right">
                <i class="glyphicon glyphicon-off"></i>
            </a>
        </div>
    </div>
</div>